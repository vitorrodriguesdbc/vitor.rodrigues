package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Seguradora {
@Id
@GeneratedValue(strategy=GenerationType.SEQUENCE)
private long id;

@Column(name="NOME")
private String nome;

@Column(name="CNPJ")
private long cnpj;

}

//========================================================


@Entity
public class Servicos {
@Id
@GeneratedValue(strategy=GenerationType.SEQUENCE)
private long id;

@Column(name="NOME")
private String nome;

@Column(name="DESCRICAO")
private String descricao;


@Column(name="VALOR_PADRAO")
private boolean valorPadrao;

}

//========================================================

@Entity
public class ServicoContratado {
@Id
@GeneratedValue(strategy=GenerationType.SEQUENCE)
private long id;

@Column(name="DESCRICAO")
private String descricao;


@Column(name="VALOR")
private boolean valor;

}
//========================================================

@Entity
public class Pessoas {
@Id
@GeneratedValue(strategy=GenerationType.SEQUENCE)
private long id;

@Column(name="NOME")
private String nome;

@Column(name="CPF")
private long cpf;

@Column(name="TELEFONE")
private long telefone;

@Column(name="PAI")
private String pai;

@Column(name="MAE")
private String mae;

@Column(name="EMAIL")
private String email;


}
//========================================================

@Entity
public class Segurados {

@Column(name="QTD_SERVICOS")
private long qtdServicos;

}
//========================================================

@Entity
public class Corretor {

@Column(name="CARGO")
private String cargo;

@Column(name="COMISSAO")
private boolean comissao;
}

//========================================================

@Entity
public class Enderecos {

@Column(name="CARGO")
private String cargo;

@Column(name="COMISSAO")
private boolean comissao;
}