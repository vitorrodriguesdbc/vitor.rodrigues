public class Dwarf extends Personagem {
    protected boolean equipado = false;

    public Dwarf(String nome) {
        super(nome);
        this.qtdDano = 10.0;
        this.vida = 110.0;
        this.inventario = new Inventario(1);
        this.inventario.adicionar(new Item(1, "Escudo"));
    }

    public String imprimirResumo() {
        return "Dwarf";
    }

    protected double calcularDano() {
        return this.equipado ? 5.0 : this.qtdDano;
    }

    public void equiparEscudo() {
        this.equipado = true;
    }
}