import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    
    private final double DELTA = 1e-9;
    
    @Test
    public void dwarfNasceCom110DeVida() {
        Dwarf dwarf = new Dwarf("Mulungrid Redmail");
        assertEquals(110.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeDezDeVida() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        // Assert
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeDezDeVidaDuasVezes() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(90.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeDezDeVidaDozeVezes() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeVidaEContinuaVivo() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }

    @Test
    public void dwarfNasceComStatus() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Assert
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }

    @Test
    public void dwarfPerdeDezDeVidaDozeVezesDeveMorrer() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni Shadowgrip");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(Status.MORTO, dwarf.getStatus());
    }


    @Test
    public void dwarfNasceComEscudoNoInventario() {
        Dwarf dwarf = new Dwarf("Gimli");
        Item esperado = new Item(1, "Escudo");
        Item resultado = dwarf.getInventario().obter(0);
        assertEquals(esperado, resultado);
    }

    @Test
    public void dwarfEquipaEscudoETomaMetadeDano() {
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        assertEquals(105.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral() {
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }
}