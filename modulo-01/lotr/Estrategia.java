import java.util.*;
public class Estrategia implements Estrategia1
{
    private ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();
    
    public ArrayList<Elfo> elfosVivos(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosSelecionados = new ArrayList<>();
        for(Elfo elfo : atacantes){
            if(elfo.getStatus().equals(Status.RECEM_CRIADO) || elfo.getStatus().equals(Status.SOFREU_DANO))
                this.exercitoDeElfos.alistar(elfo);
        }
        elfosSelecionados = exercitoDeElfos.getElfos();
        return elfosSelecionados;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosSelecionados = new ArrayList<>();
        elfosSelecionados = elfosVivos(atacantes);
        for (int i = 0; i < elfosSelecionados.size(); i++) {
            for (int j = 0; j < elfosSelecionados.size() - 1; j++) {
                Elfo atual = elfosSelecionados.get(j);
                Elfo proximo = elfosSelecionados.get(j + 1);

                if (atual instanceof ElfoNoturno ) {
                    Elfo itemTrocado = atual;
                    elfosSelecionados.set(j, proximo);
                    elfosSelecionados.set(j + 1, itemTrocado);
                }
            }
        }
        return elfosSelecionados;
    }

    public ArrayList<Elfo> getOrdemAlternadaDeAtaque(ArrayList<Elfo> atacantes){
        Elfo primeiroDoBatalhao = atacantes.get(0);
        ArrayList<Elfo> elfosSelecionados = new ArrayList<>();
        elfosSelecionados = elfosVivos(atacantes);
        int i = (primeiroDoBatalhao instanceof ElfoVerde) ? 1 : 0;
        int j = (primeiroDoBatalhao instanceof ElfoVerde) ? 0 : 1;
            for(Elfo elfo : atacantes){
                if(elfo instanceof ElfoVerde){
                    elfosSelecionados.add(j,elfo);
                    j += 2;
                }
                if(elfo instanceof ElfoNoturno){
                    elfosSelecionados.add(i,elfo);
                    i += 2;
                }
            }
        return elfosSelecionados;
    }
    
}
