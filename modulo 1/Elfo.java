public class Elfo{
    
    private String nome; 
    private int experiencia=0;
    private Inventario inventElfo= new Inventario();
    private Status status;
    
    {
    status = Status.RECEM_CRIADO;
    
    }
    public Elfo (String nome){
        setNome(nome);
        gerarItensIniciaisDeElfos();
       
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public Status getStatus(){
        return status.RECEM_CRIADO;
    }
    
    public void setNome(String nome){
        this.nome= nome;
    }
    
    public int getExperiencia()
    {
        return experiencia;
    }
    
    private void aumentarXp(){
        experiencia++;
    }
    
    public int getFlecha(){
     return inventElfo.obterItemNaPosicao(0).getQuantidade();
    }
    
    private boolean podeAtirarFlechas()
    {
        return inventElfo.obterItemNaPosicao(0).getQuantidade()>0;
    }
    
    public void atirarFlechaEmDwarf(Dwarf dwarf)
    {   int qtdAtual = getFlecha();   
        if(podeAtirarFlechas()){

            inventElfo.obterItemNaPosicao(0).setQuantidade(qtdAtual-1);
            aumentarXp();
            dwarf.sofrerDano();
            
        }
    }
    
    public void gerarItensIniciaisDeElfos(){
        Item flecha = new Item (2,"flechas");
        inventElfo.adicionarItemAoInventario(flecha);
        Item arco = new Item(1,"arco");
        inventElfo.adicionarItemAoInventario(arco);
    }
    
    
    
}