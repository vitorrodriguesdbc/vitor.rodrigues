import java.util.*;
public class AgendaContatos
{
   private HashMap<String, Contato> agenda;
   public AgendaContatos(){
    agenda = new HashMap<>();
   }
   
   private void adicionar(Contato cont){
    agenda.put(cont.getNome(),cont);
    }
   private int buscar (String nome){
    return agenda.get(nome).getNumero();
    }  
}
