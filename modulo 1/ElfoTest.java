import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
     @Test
     public void atirarFlechaDevePerderFlechaAumentarXpTirarVidaDoDwarf2()
     {
          Elfo umElfo = new Elfo("Legolas");
          Dwarf dwarf= new Dwarf("Umdwarf");
          umElfo.atirarFlechaEmDwarf(dwarf);
          umElfo.atirarFlechaEmDwarf(dwarf);
          umElfo.atirarFlechaEmDwarf(dwarf);
          assertEquals(2,umElfo.getExperiencia());
          assertEquals(0,umElfo.getFlecha());
          assertEquals(90,dwarf.getVida(),.000001);
     }
     @Test
     public void atirarFlechaDevePerderFlechaAumentarXpTirarVidaDoDwarf()
     {
          Elfo umElfo = new Elfo("Legolas"); 
          Dwarf dwarf= new Dwarf("Umdwarf");
          umElfo.atirarFlechaEmDwarf(dwarf);
          assertEquals(1,umElfo.getExperiencia());
          assertEquals(1,umElfo.getFlecha());
          assertEquals(100.0,dwarf.getVida(), .000001);
     }
     @Test
     public void criarElfosDeveTerNumeroExtatoDeFlechas()
     {
          Elfo umElfo = new Elfo("Legolas"); 
          
          assertEquals(2,umElfo.getFlecha());
      
     }
     @Test
     public void atirarFlechaDevePerderFlecha()
     {
          Elfo umElfo = new Elfo("Legolas"); 
          Dwarf dwarf= new Dwarf("Umdwarf");
          
          
          umElfo.atirarFlechaEmDwarf(dwarf);
          
          assertEquals(1,umElfo.getFlecha());
          
     }
     @Test
     public void ElfoNasceStatusRecemCriado(){
        Elfo elfo = new Elfo("um Elfo");
        assertEquals(Status.RECEM_CRIADO,elfo.getStatus());
        }
     
}
