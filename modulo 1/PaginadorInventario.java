public class PaginadorInventario
{
  private Inventario inventPag= new Inventario();
  private String descricoes= "" ;
  private int marcadorInicial;      
  
  public PaginadorInventario(Inventario invent){
    
    inventPag= invent;
    
    }
  public int pular(int inicial){
      marcadorInicial= inicial; 
      return inicial;
    }  
  public String limitar(int limite){
      descricoes= descricoes +inventPag.paginacaoItens(marcadorInicial,limite);
      return descricoes;
    }  
  
}
