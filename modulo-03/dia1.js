var moedas = 
(function(){
  function retornaValorQuebrado(val){
    var valInteiro= parseInt(val);
    var valQueb= val- valInteiro;
 
    valQueb = parseFloat(valQueb).toFixed(2)*100
    if(valQueb<10){
       valQueb= "0" + valQueb;
    }
    return valQueb;
    }
 
 function retornaValorInteiro(val, separador){
       var valComPontos=""
       var valInteiro= parseInt(val);
       var cont =0;
       for(var c = valInteiro.toString().length-1;c>=0;c--){
 
          if(cont===3){
             cont=0;
             valComPontos= separador + valComPontos;
          }
          cont++;
          valComPontos=  valInteiro.toString().charAt(c)+ valComPontos;
 
       }
    return valComPontos;
 }  

return{    imprimirBRL:function(valor){
      return "R$ " +retornaValorInteiro(valor, ".")+","+retornaValorQuebrado(valor).toString();
    },
    imprimirFR:function(valor){
      return retornaValorInteiro(valor,".")+","+retornaValorQuebrado(valor).toString()+" €";
    },
    imprimirGBP:function(valor){
      return "£ " +retornaValorInteiro(valor,",")+"."+retornaValorQuebrado(valor).toString();
    }
  }
})()


/* var adicionar = op1 => op2=> op1+op2;
console.log(`o valor de x é: ${ x }`)

(function teste(){})() //IIFE

var nome= "Bernardo";
var cidade= "Porto Alegre";
var objeto={
   nome,
   cidade
} */

//function(params){ const {separadoDecimal, separadorMilhar} = params}
