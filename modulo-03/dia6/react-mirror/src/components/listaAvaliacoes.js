import React from 'react'
import ListaEpisodiosUi from './shared/listaEpisodiosUi'

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
  return <ListaEpisodiosUi listaEpisodios={ listaEpisodios.avaliados } />
  }

export default ListaAvaliacoes
