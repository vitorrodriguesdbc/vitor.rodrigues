import React from 'react'
import './listaEpisodiosUi.css'
const ListaEpisodiosUi = ( { listaEpisodios } ) => {
  return(
   listaEpisodios.map( e => <li key={ e.id }><a href="/telaDetalheEpisodioUi">{ `${ e.nome } - ${ e.nota }` }</a></li> )
   )
}

export default ListaEpisodiosUi