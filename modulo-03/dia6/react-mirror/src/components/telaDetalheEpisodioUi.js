import React,{Component} from 'react'
import EpisodiosApi from '../models/episodiosApi'
import DetalheEpisodio from '../models/detalheEpisodio'
import EpisodioUi from './episodioUi'
import ListaEpisodios from '../models/listaEpisodios'

export default class TelaDetalheEpisodio extends Component {
  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      detalheEpisodio: {},
      episodio:{}
    }
    
  }
  componentDidMount() {
    this.episodiosApi.buscarDetalhes(14)
      .then( detalhesDoServidor => {
        this.setState( {
          detalheEpisodio: new DetalheEpisodio( detalhesDoServidor.data[ 0 ] )
        } )
      },
      this.episodiosApi.buscar()
      .then( episodiosDoServidor => {
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor.data )
        this.setState( { 
            episodio: this.listaEpisodios.episodioAleatorio 
          } )
        
      } ) )
      
  }
  
  render(){
    const { detalheEpisodio,episodio} = this.state
    return (
      !detalheEpisodio ? (
        <h3>Aguarde...</h3>
      ) : (
    <React.Fragment>
        <EpisodioUi episodio={ episodio } />
         <p>{detalheEpisodio.sinopse}</p>
         <span>{detalheEpisodio.dataEstreia}</span>
         <span>{detalheEpisodio.notaImdb}</span>
    </React.Fragment>
      )
    )
  }
}