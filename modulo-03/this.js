// registra evento quando carrega os elementos do HTML em memória
let executaIssoQuandoCarregar = function() {
  let h1Titulo = document.getElementById('titulo')
  h1Titulo.innerText = 'Kamisama pode ser cruel'
}
document.addEventListener('DOMContentLoaded', executaIssoQuandoCarregar  )

var luke = {
  nome: "Luke Skywalker",
  idade: 23,
  imprimirInformacoes: function(forca, irma) {
    console.log(`arguments: ${ arguments[0] } ${ arguments[1] }`)
    return `${ this.nome } ${ this.idade } ${ forca } ${ irma }`
  }
}

var outraFunc = luke.imprimirInformacoes

console.log(luke.imprimirInformacoes()) // "Luke Skywalker 23"
console.log(outraFunc()) // "undefined undefined"

window.nome = "Nome global"
console.log(outraFunc()) // "Nome global undefined"

console.log(outraFunc.call(luke, 45, 'Princesa Leia')) // "Luke Skywalker 23"
console.log(outraFunc.apply(luke, [ 45, 'Princesa Leia' ])) // "Luke Skywalker 23"

var outraFuncComBind = luke.imprimirInformacoes.bind( luke )
console.log( `com bind: ${ outraFuncComBind() }` )
window.setTimeout( function() {
  console.log( `dentro do setTimeout: ${ this.nome }` )
}, 2000 )
