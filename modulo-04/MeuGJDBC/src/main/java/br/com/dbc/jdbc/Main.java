/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitor.rodrigues
 */
public class Main {

    public static void main(String[] args) {
        Connection conn = Connector.connect();
        try {
           ResultSet rs =conn.prepareStatement("select tname from tab where tname = 'USUARIO'").executeQuery();
           if(!rs.next()){
           conn.prepareStatement("CREATE TABLE USUARIO(\n"
                    +"ID NUMBER NOT NULL PRIMARY KEY,"
                    + "NOME VARCHAR(100) NOT NULL,"
                    + "APELIDO VARCHAR(15) NOT NULL,"
                    + "SENHA VARCHAR(15) NOT NULL,"
                    + "CPF NUMBER NOT NULL"
                    +")").execute();
           }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na consulta main", ex);
        }
    }
}
