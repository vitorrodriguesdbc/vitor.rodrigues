/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.entity.BaseEntity;
import br.com.dbc.jogos.entity.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author tiago
 */
public abstract class AbstractDAO<E extends BaseEntity> {

    protected abstract Class<E> getEntityClass();

    public void saveOrUpdate(E e) {
        Session session = HibernateUtil.getSession(true);
        session.save(e);
        HibernateUtil.commitTransaction();
    }

    public E findById(Integer id) {
        if (id == null)
            return null;
        Session session = HibernateUtil.getSession();
        return (E) session.createCriteria(getEntityClass()).add(Restrictions.idEq(id)).uniqueResult();
    }

    public List<E> findAll() {
        Session session = HibernateUtil.getSession();
        return session.createCriteria(getEntityClass()).list();
    }

    public void delete(Integer id) {
        if (id == null)
            return;
        Session session = HibernateUtil.getSession(true);
        session.delete(findById(id));
        HibernateUtil.commitTransaction();
    }

}
