package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;


import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;

@RunWith(SpringRunner.class)
public class SeguradoraIntegration2Tests {
	
	@MockBean
	private SeguradoraRepository seguradoraRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Seguradora seguradora = new Seguradora();
		seguradora.setNome("Lalala");
		
		Mockito.when(seguradoraRepository.findByNome(seguradora.getNome())).thenReturn(seguradora);
	}
	
	@Test
	public void buscarPorNome() {
		String nome = "Lalala";
		Seguradora found = seguradoraRepository.findByNome(nome);
		assertThat(found.getNome()).isEqualTo(nome);
	}
}
