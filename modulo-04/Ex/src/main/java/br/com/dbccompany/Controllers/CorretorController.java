package br.com.dbccompany.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Service.CorretorService;


@Controller
@RequestMapping("/api/corretor")
public class CorretorController {
	
	@Autowired
	 public CorretorService corretorService;
	
	@PostMapping(value="/novo")
	@ResponseBody
	public Corretor novaCorretor(@RequestBody Corretor corretor) {
		return corretorService.salvar(corretor);
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public Corretor buscarPorId(@PathVariable long codigo) {
		return corretorService.buscarPorCodigo(codigo);
	}
}
