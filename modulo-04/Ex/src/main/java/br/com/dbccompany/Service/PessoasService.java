package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Repository.PessoasRepository;


@Service
public class PessoasService {
		
	@Autowired
	public PessoasRepository pessoasRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas salvar(Pessoas pessoa) {
		
		return pessoasRepository.save(pessoa);	
	}

	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorNome(String nome){

		return pessoasRepository.findByNome(nome);
	}

	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorCpf(long cpf){

		return pessoasRepository.findByCpf(cpf);
	}
	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorEmail(String email){

		return pessoasRepository.findByEmail(email);
	}


	public Pessoas buscarPorCodigo(long id) {
		return pessoasRepository.findById(id).get();
	}
	
}
