package br.com.dbccompany.Security;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	protected void configure(HttpSecurity http) throws Exception{
		http.csrf().disable().authorizeRequests()
		//quais telas tem permissao
		.antMatchers(HttpMethod.POST,"/login").permitAll()
		.antMatchers("/api/seguradora/**").permitAll()
		//.antMatchers("/api/servicocontratado/novo").authenticated()
		.anyRequest().authenticated()
		.and()
		.addFilterAfter(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
		.addFilterAfter(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		
	}
	//usuario em memoria
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		auth.inMemoryAuthentication()
		.withUser("admin")
		.password("{noop}password")
		.roles("ADMIN");
	}
}
