package br.com.dbccompany.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Service.ServicosService;

@Controller
@RequestMapping("/api/servicos")
public class ServicosController {
	
	@Autowired
	 public ServicosService servicosServices;
	
	@PostMapping(value="/novo")
	@ResponseBody
	public Servicos novoServico(@RequestBody Servicos servicos) {
		return servicosServices.salvar(servicos);
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public Servicos buscarPorId(@PathVariable long codigo) {
		return servicosServices.buscarPorCodigo(codigo);
	}
}
