package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Segurados;

public interface SeguradosRepository extends CrudRepository<Segurados, Long>{
	
}
