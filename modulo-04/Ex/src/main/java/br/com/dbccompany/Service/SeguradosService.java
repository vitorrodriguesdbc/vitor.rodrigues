package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Repository.SeguradosRepository;

@Service
public class SeguradosService {
	@Autowired
	public SeguradosRepository seguradosRepository;
	
	
	public Segurados buscarPorCodigo(long id) {
		return seguradosRepository.findById(id).get();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados salvar(Segurados seguradora) {
		
		return seguradosRepository.save(seguradora);
		
	}
}
