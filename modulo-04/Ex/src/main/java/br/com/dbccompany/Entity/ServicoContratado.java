package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class ServicoContratado {
	@Id
	@SequenceGenerator(name="SERVICO_CONTRATADO_SEQ",sequenceName="SERVICO_CONTRATADO_SEQ")
	@GeneratedValue(generator="SERVICO_CONTRATADO_SEQ",strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	
	@Column(name="VALOR")
	private double valor;
	
	@ManyToOne
	@JoinColumn(name="id_servicos")
	private Servicos servico;
	
	@ManyToOne
	@JoinColumn(name="id_seguradora")
	private Seguradora seguradora;
	
	@ManyToOne
	@JoinColumn(name="id_pessoas_segurados")
	private Segurados pessoaSegurada;
	
	@ManyToOne
	@JoinColumn(name="id_pessoas_corretor")
	private Corretor pessoaCorretora;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Servicos getServico() {
		return servico;
	}

	public void setServico(Servicos servico) {
		this.servico = servico;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	public Segurados getPessoaSegurada() {
		return pessoaSegurada;
	}

	public void setPessoaSegurada(Segurados pessoaSegurada) {
		this.pessoaSegurada = pessoaSegurada;
	}

	public Corretor getPessoaCorretora() {
		return pessoaCorretora;
	}

	public void setPessoaCorretora(Corretor pessoaCorretora) {
		this.pessoaCorretora = pessoaCorretora;
	}
	
}
