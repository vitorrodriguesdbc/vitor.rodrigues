package br.com.dbccompany.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Service.ServicoContratadoService;

@Controller
@RequestMapping("/api/servicocontratado")
public class ServicoContratadoController {
	

	@Autowired
	 public ServicoContratadoService servicoContratadoService;
	
	@PostMapping(value="/novo")
	@ResponseBody
	public ServicoContratado novoServicoContratado(@RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.salvar(servicoContratado);
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public ServicoContratado buscarPorId(@PathVariable long codigo) {
		return servicoContratadoService.buscarPorCodigo(codigo);
	}
}
