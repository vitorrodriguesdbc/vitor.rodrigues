package br.com.dbccompany.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Service.SeguradosService;

@Controller
@RequestMapping("/api/segurados")
public class SeguradosController {
	
	@Autowired
	 public SeguradosService seguradosServices;
	
	@PostMapping(value="/novo")
	@ResponseBody
	public Segurados novoSegurado(@RequestBody Segurados segurado) {
		return seguradosServices.salvar(segurado);
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public Segurados buscarPorId(@PathVariable long codigo) {
		return seguradosServices.buscarPorCodigo(codigo);
	}
}
