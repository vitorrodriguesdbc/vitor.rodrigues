package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Corretor extends Pessoas {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="CARGO")
	private String cargo;
	
	@Column(name="COMISSAO")
	private boolean comissao;
	
	@OneToMany (mappedBy = "pessoaCorretora", cascade = CascadeType.ALL)
	private List<ServicoContratado> servicosContrados = new ArrayList<>();


	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public boolean getComissao() {
		return comissao;
	}

	public void setComissao(boolean comissao) {
		this.comissao = comissao;
	}

	public List<ServicoContratado> getServicosContrados() {
		return servicosContrados;
	}

	public void setServicosContrados(List<ServicoContratado> servicosContrados) {
		this.servicosContrados = servicosContrados;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
