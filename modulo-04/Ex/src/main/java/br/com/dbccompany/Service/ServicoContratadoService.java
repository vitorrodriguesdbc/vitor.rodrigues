package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.ServicoContratadoRepository;

@Service
public class ServicoContratadoService {
	
	@Autowired
	public ServicoContratadoRepository servicoContratadoRepository;
	
	
	public ServicoContratado buscarPorCodigo(long id) {
		return servicoContratadoRepository.findById(id).get();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado salvar(ServicoContratado servicoContratado) {
		
		return servicoContratadoRepository.save(servicoContratado);
		
	}
}
