package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Repository.ServicosRepository;

@Service
public class ServicosService {
	
	@Autowired
	public ServicosRepository servicosRepository;
	
	
	public Servicos buscarPorCodigo(long id) {
		return servicosRepository.findById(id).get();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos salvar(Servicos servicos) {
		
		return servicosRepository.save(servicos);
		
	}
}
