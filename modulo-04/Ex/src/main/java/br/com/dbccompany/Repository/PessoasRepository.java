package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Pessoas;

public interface PessoasRepository extends CrudRepository<Pessoas, Long>{
	
	Pessoas findByNome(String nome);
	Pessoas findByCpf(long cpf);
	Pessoas findByEmail(String email);
}
