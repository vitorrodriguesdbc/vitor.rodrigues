
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vitor.rodrigues
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "POKEMON_SEQ", sequenceName = "POKEMON_SEQ")
public class Pokemon {
    @Id
    @GeneratedValue(generator = "POKEMON_SEQ")
    private Integer id;
    
    @OneToOne(mappedBy = "pokemon")
    private Charmander chamander ;
}
