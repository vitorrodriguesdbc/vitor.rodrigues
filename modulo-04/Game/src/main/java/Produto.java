
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author vitor.rodrigues
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "PRODUTO_SEQ", sequenceName = "PRODUTO_SEQ")

public class Produto {

    @Id
    @GeneratedValue(generator = "PRODUTO_SEQ", strategy = GenerationType.SEQUENCE)

    private Integer id;

    @ManyToMany(mappedBy = "produtos")
    private List<Conta2> contas;
}
