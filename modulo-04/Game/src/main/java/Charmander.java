
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vitor.rodrigues
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "CHARMANDER_SEQ", sequenceName = "CHARMANDER_SEQ")
public class Charmander {
    @Id
    @GeneratedValue(generator = "CHARMANDER_SEQ")
    private Integer id;
    
    @OneToOne(cascade = CascadeType.ALL)
    private Pokemon pokemon ;
    
}
