/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dto;

import br.com.dbc.lotr.entity.RacaType;

/**
 *
 * @author vitor.rodrigues
 */
public class PersonagemDTO {
    
    private Integer id;
    private String nome;
    private RacaType raca;
    private double danoElfo;
    private Double danoHobiit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public RacaType getRaca() {
        return raca;
    }

    public void setRaca(RacaType raca) {
        this.raca = raca;
    }

    public double getDanoElfo() {
        return danoElfo;
    }

    public void setDanoElfo(double danoElfo) {
        this.danoElfo = danoElfo;
    }

    public Double getDanoHobiit() {
        return danoHobiit;
    }

    public void setDanoHobiit(Double danoHobiit) {
        this.danoHobiit = danoHobiit;
    }
    
}
