package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        dto.setApelidoUsuario("jose");
        dto.setNomeUsuario("jose");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("1234");
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setBairro("do jose");
        enderecoDTO.setCidade("do jose");
        enderecoDTO.setComplemento("casa");
        enderecoDTO.setLogradouro("do jose");
        enderecoDTO.setNumero(1234);
        dto.setEnderecosUsuario(enderecoDTO);
        PersonagemDTO pesonagemDTO = new PersonagemDTO();
        pesonagemDTO.setRaca(RacaType.ELFO);
        pesonagemDTO.setDanoElfo(100d);
        pesonagemDTO.setNome("zezinho");
        dto.setPersonagem(pesonagemDTO);
        usuarioService.cadastrarUsuarioPersonagem(dto);
        System.exit(0);
    }

    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

            Usuario usuario = new Usuario();
            usuario.setNome("Antonio");
            usuario.setApelido("tonho");
            usuario.setCpf(123L);
            usuario.setSenha("1234");

            Endereco enderecos = new Endereco();
            enderecos.setLogradouro("aa");
            enderecos.setCidade("cidade do antonio");
            enderecos.setBairro("bairro do antonio");
            enderecos.setComplemento("aa");
            enderecos.setNumero(712);

            usuario.pushEnderecos(enderecos);
            session.save(usuario);

            ElfoTabelao elfo = new ElfoTabelao();
            elfo.setDanoElfo(100d);
            elfo.setNome("lucio");
            session.save(elfo);

            HobbitTabelao hobbit = new HobbitTabelao();
            elfo.setDanoElfo(100d);
            elfo.setNome("lucio");
            session.save(hobbit);

            ElfoPerClass elfo1 = new ElfoPerClass();
            elfo1.setDanoElfo(100d);
            elfo1.setNome("lucio");
            session.save(elfo1);

            ElfoJoin elfo2 = new ElfoJoin();
            elfo2.setDanoElfo(100d);
            elfo2.setNome("lucio");
            session.save(elfo2);

            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.and(
                    Restrictions.ilike("endereco.bairro", "%do antonio"),
                    Restrictions.ilike("endereco.cidade", "%do antonio")
            ));
            List<Usuario> usuarios = criteria.list();
            usuarios.forEach(System.out::println);

            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.and(
                    Restrictions.ilike("endereco.bairro", "%do antonio"),
                    Restrictions.ilike("endereco.cidade", "%do antonio")
            ));
            criteria.setProjection(Projections.rowCount());

            System.out.println(String.format("Foram encontrado(s) " + criteria.uniqueResult() + " registro(s)"));

            usuarios = session.createQuery("select u from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ")
                    .list();

            usuarios.forEach(System.out::println);

            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                System.exit(1);
            }
        } finally {
            if (session != null) {
                session.close();
            }
            System.exit(0);
        }

    }
}
