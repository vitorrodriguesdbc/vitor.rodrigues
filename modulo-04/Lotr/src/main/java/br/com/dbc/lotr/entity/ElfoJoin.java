/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author vitor.rodrigues
 */
@Entity
@Table(name = "ELFO_JOIN_CLASS")
@PrimaryKeyJoinColumn(name ="ID_PERSONAGEM" )
public class ElfoJoin extends PersonagemJoin {

    public ElfoJoin() {
        setRaca(RacaType.ELFO);
    }

    @Column(name = "DANO_ELFO")
    private double danoElfo;

    public double getDanoElfo() {
        return danoElfo;
    }

    public void setDanoElfo(double danoElfo) {
        this.danoElfo = danoElfo;
    }

}
