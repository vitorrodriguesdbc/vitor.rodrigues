/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author vitor.rodrigues
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
public class Conta {
    @Id
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    
    private Integer id;
    private Integer numero;
    private String senha;
    
}
