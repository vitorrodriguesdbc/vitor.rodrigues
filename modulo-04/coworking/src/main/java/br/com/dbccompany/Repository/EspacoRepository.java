package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.EspacoPacote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacoRepository extends CrudRepository<Espaco, Long> {
    Espaco findByNome(String nome);
    Espaco findByQtdPessoas(long qtdPessoas);
    Espaco findByValor(double valor);
    Espaco findByEspacosPacotes(EspacoPacote espacoPacote);
    Espaco findByContratacoes(Contratacao contratacao);
}
