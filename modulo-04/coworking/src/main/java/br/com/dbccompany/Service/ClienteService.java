package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    ContatoRepository contatoRepository;

    @Autowired
    TipoContatoRepository tipoContatoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Cliente salvar( Cliente cliente ) throws Exception{

        if(cliente.getContatos().isEmpty()){
            throw new Exception("Cliente deve possuir contato!");
        }
        for (int i=0; i< cliente.getContatos().size(); i++) {
            if (!cliente.getContatos().get(i).getTipoContato().getNome().equals("email")
                    || !cliente.getContatos().get(i).getTipoContato().getNome().equals("telefone")) {
                throw new Exception("Cliente deve possuir no minimo um email e um telefone!");
            }
        }

        if(clienteRepository.findByCpf(cliente.getCpf())!=null){
            throw new Exception("Já existe um usuario com esse CPF!");
        }
        if (cliente.getCpf()==null){
            throw new Exception("Campo CPF é obrigatorio!");
        }
        if (cliente.getDataNascimento()==null){
            throw new Exception("Campo data de nascimento é obrigatorio!");
        }
        if (cliente.getNome()==null){
            throw new Exception("Campo Nome é obrigatorio!");
        }

        return clienteRepository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente editarCliente( long id, Cliente cliente ) {
        cliente.setId( id );
        return clienteRepository.save( cliente );
    }

    public List<Cliente> allClientes() {
        return (List<Cliente>) clienteRepository.findAll();
    }

    public Optional<Cliente> buscarCliente(long id ){
        return clienteRepository.findById(id);
    }

}
