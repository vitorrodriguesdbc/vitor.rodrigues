package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService contratacaoService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Contratacao> listPagamentos(){
        return contratacaoService.allContratacoes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public String novaContratacao( @RequestBody Contratacao contratacao ) {
        return contratacaoService.salvar(contratacao);
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Optional<Contratacao> ContratacaoEspecifica(@PathVariable long id) {
        return contratacaoService.buscarContratacao(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contratacao editarContratacao( @PathVariable long id, @RequestBody Contratacao contratacao ) {
        return contratacaoService.editarContratacao(id, contratacao);
    }
}
