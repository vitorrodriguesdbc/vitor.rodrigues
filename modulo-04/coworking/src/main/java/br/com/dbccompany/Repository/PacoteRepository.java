package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacoteRepository extends CrudRepository<Pacote, Long> {
    Pacote findByValor(double valor);
    Pacote findByEspacosPacotes(EspacoPacote espacoPacote);
    Pacote findByPacotesClientes(ClientePacote clientePacote);
}
