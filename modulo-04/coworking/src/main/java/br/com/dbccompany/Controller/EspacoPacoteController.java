package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/espacoPacote")
public class EspacoPacoteController {
    @Autowired
    EspacoPacoteService espacoPacoteService;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EspacoPacote novaContratacao(@RequestBody EspacoPacote espacoPacote ) {
        return espacoPacoteService.salvar(espacoPacote);
    }
}
