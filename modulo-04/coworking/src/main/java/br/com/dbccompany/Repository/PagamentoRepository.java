package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findByClientePacote(ClientePacote clientePacote);
    List<Pagamento> findByContratacao(Contratacao contratacao);
    Pagamento findByTipoPagamento(TipoPagamento tipoPagamento);
}
