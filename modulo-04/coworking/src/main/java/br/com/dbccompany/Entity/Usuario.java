package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Usuario.class)
@Table( name = "USUARIOS" )
public class Usuario {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ",
            sequenceName = "USUARIOS_SEQ")
    @GeneratedValue(generator = "USUARIOS_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "EMAIL", nullable = false, unique = true )
    private String email;

    @Column( name = "LOGIN", nullable = false, unique = true )
    private String login;

    @Column( name = "SENHA", nullable = false )
    @Length( min = 6 )
    private String senha;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
