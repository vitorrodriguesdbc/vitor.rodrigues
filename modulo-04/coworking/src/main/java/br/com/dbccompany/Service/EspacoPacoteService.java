package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.EspacoPacoteRepository;
import br.com.dbccompany.Repository.EspacoRepository;
import br.com.dbccompany.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EspacoPacoteService {

    @Autowired
    EspacoPacoteRepository espacoPacoteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    PacoteRepository pacoteRepository;


    @Transactional(rollbackFor = Exception.class)
    public EspacoPacote salvar(EspacoPacote espacoPacote ) {
        Espaco espaco = espacoRepository.findById(espacoPacote.getEspaco().getId()).get();
        espacoPacote.setEspaco(espaco);

        Pacote pacote = pacoteRepository.findById(espacoPacote.getPacote().getId()).get();
        espacoPacote.setPacote(pacote);

        return espacoPacoteRepository.save(espacoPacote);
    }
}

