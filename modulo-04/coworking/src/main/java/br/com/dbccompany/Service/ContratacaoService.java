package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.ContratacaoRepository;
import br.com.dbccompany.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ContratacaoService {
    @Autowired
    ContratacaoRepository contratacaoRepository;
    @Autowired
    EspacoRepository espacoRepository;
    @Autowired
    ClienteRepository clienteRepository;


    @Transactional( rollbackFor = Exception.class )
    public String salvar(Contratacao contratacao ) {
        Espaco espaco = espacoRepository.findById(contratacao.getEspaco().getId()).get();
        contratacao.setEspaco(espaco);

        Cliente cliente = clienteRepository.findById(contratacao.getCliente().getId()).get();
        contratacao.setCliente(cliente);

        long qtd = contratacao.getQuantidade();
        double valor = espaco.getValor();

        contratacaoRepository.save(contratacao);

        return String.format("R$: %.2f", qtd*valor);
    }

    @Transactional( rollbackFor = Exception.class )
    public Contratacao editarContratacao( long id, Contratacao contratacao ) {
        contratacao.setId(id);
        return contratacaoRepository.save( contratacao );
    }

    public List<Contratacao> allContratacoes() {
        return (List<Contratacao>) contratacaoRepository.findAll();
    }

    public Optional<Contratacao> buscarContratacao(long id ){
        return contratacaoRepository.findById(id);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContratacaoService)) return false;
        ContratacaoService that = (ContratacaoService) o;
        return Objects.equals(contratacaoRepository, that.contratacaoRepository) &&
                Objects.equals(espacoRepository, that.espacoRepository) &&
                Objects.equals(clienteRepository, that.clienteRepository);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contratacaoRepository, espacoRepository, clienteRepository);
    }
}
