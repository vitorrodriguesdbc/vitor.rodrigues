package br.com.dbccompany.Entity;

import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SaldoClienteId implements Serializable {

    @ManyToOne(cascade  = CascadeType.MERGE)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    @ManyToOne(cascade  = CascadeType.MERGE)
    @JoinColumn(name = "id_espaco")
    private Espaco espaco;

    public SaldoClienteId() {

    }

    public SaldoClienteId(Cliente cliente, Espaco espaco) {
        this.cliente = cliente;
        this.espaco = espaco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SaldoClienteId)) return false;
        SaldoClienteId that = (SaldoClienteId) o;
        return Objects.equals(getCliente(), that.getCliente()) &&
                Objects.equals(getEspaco(), that.getEspaco());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCliente(), getEspaco());
    }
}
