package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long> {
    Cliente findByNome(String nome);
    Cliente findByCpf(String cpf);
    Cliente findByDataNascimento(Date dataNascimento);
    Cliente findByContatos(Contato contato);
    Cliente findByClientePacotes(ClientePacote clientePacote);
    Cliente findByContratacoes(Contratacao contratacao);
}
