package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<Contato, Long> {
    Contato findByValor(String valor);
    List<Contato> findByTipoContato(TipoContato tipoContato);
    List<Contato> findByCliente(Cliente cliente);
}
