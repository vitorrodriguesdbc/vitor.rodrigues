package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {
    @Autowired
    EspacoService espacoService;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Espaco novaContratacao(@RequestBody Espaco espaco ) throws Exception{
        return espacoService.salvar(espaco);
    }

}
