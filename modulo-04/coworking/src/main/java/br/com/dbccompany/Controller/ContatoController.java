package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("api/contato")
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Contato novo(@RequestBody Contato contato ) {
        return contatoService.salvar(contato);
    }
}
