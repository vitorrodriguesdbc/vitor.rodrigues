package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/api/pacote")
public class PacoteController {

    @Autowired
    PacoteService pacoteService;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Pacote novaContratacao( @RequestBody Pacote contratacao ) {
        return pacoteService.salvar(contratacao);
    }
}
