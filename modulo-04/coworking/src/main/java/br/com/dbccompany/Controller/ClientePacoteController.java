package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/clientePacote")
public class ClientePacoteController {
    @Autowired
    ClientePacoteService clientePacoteService;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientePacote novoClientePacote(@RequestBody ClientePacote clientePacote ) {
        return clientePacoteService.salvar(clientePacote);
    }
}
