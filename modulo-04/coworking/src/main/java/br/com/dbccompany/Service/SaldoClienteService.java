package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {
    @Autowired
    SaldoClienteRepository saldoClienteRepository;


    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar( SaldoCliente saldoCliente ) {
        return saldoClienteRepository.save(saldoCliente);
    }

    public List<SaldoCliente> allSaldoClientes() {
        return (List<SaldoCliente>) saldoClienteRepository.findAll();
    }

    public Optional<SaldoCliente> buscarSaldoCliente(long id ){
        return saldoClienteRepository.findById(id);
    }

}
