package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = ClientePacote.class)
@Table( name = "CLIENTES_PACOTES" )
public class ClientePacote {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_PACOTES_SEQ",
            sequenceName = "CLIENTES_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_PACOTES_SEQ",
            strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_Cliente" )
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_Pacote" )
    private Pacote pacote;

    @Column( name = "QUANTIDADE" )
    private long quantidade;

    @OneToMany( mappedBy = "clientePacote", cascade = CascadeType.MERGE )
    private List<Pagamento> pagamentos = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public List<Pagamento> getPagamentos() {
        return pagamentos;
    }

    public void pushPagamentos(Pagamento... pagamentos) {
        this.pagamentos.addAll(Arrays.asList(pagamentos));
    }
}
