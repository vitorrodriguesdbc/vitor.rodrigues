package br.com.dbccompany;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Service.EspacoService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith( SpringRunner.class )
public class EspacoIntegrationTest {

    @MockBean
    EspacoService espacoService;

    @Test
    public void nomeUnico(){
        Espaco espaco = new Espaco();
        espaco.setNome("dbc");
        espaco.setQtdPessoas(250);
        espaco.setValor(800000);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("dbc");
        espaco1.setQtdPessoas(890);
        espaco1.setValor(500);

        try {
            espacoService.salvar(espaco);
            espacoService.salvar(espaco1);

        }catch (Exception e){
            Assert.assertEquals("Nome já existe", e.getMessage());
        }
    }

    @Test
    public void nomeVazio(){

        Espaco espaco = new Espaco();
        espaco.setQtdPessoas(250);
        espaco.setValor(800000);

        try {
            espacoService.salvar(espaco);

        }catch (Exception e){
            Assert.assertEquals("Nome é obrigatorio", e.getMessage());
        }
    }

    @Test
    public void qtdPessoasVazio(){

        Espaco espaco = new Espaco();
        espaco.setNome("dbc");
        espaco.setValor(800000);

        try {
            espacoService.salvar(espaco);

        }catch (Exception e){
            Assert.assertEquals("Quantidade de pessoas é obrigatorio", e.getMessage());
        }
    }

    @Test
    public void valorVazio(){

        Espaco espaco = new Espaco();
        espaco.setNome("dbc");
        espaco.setQtdPessoas(500);

        try {
            espacoService.salvar(espaco);

        }catch (Exception e){
            Assert.assertEquals("Valor é obrigatorio", e.getMessage());
        }
    }
}
