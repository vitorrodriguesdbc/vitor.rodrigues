package br.com.dbccompany;

import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Service.PacoteService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.Month;

@RunWith( SpringRunner.class )
public class PacoteIntegrationTest {

    @MockBean
    PacoteService pacoteService;

    @Test
    public void deveGerarListaDeEspacos(){

        Espaco espaco = new Espaco();
        espaco.setNome("dbc");
        espaco.setQtdPessoas(250);
        espaco.setValor(800000);

        Espaco espaco1 = new Espaco();
        espaco.setNome("joao");
        espaco.setQtdPessoas(250);
        espaco.setValor(800);

        EspacoPacote espacoPacote =new EspacoPacote();
        espacoPacote.setEspaco(espaco);

        EspacoPacote espacoPacote1 =new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);



        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote);
        pacote.pushEspacosPacotes(espacoPacote1);

        Assert.assertEquals(pacote.getEspacosPacotes().size(),2);

    }

    @Test
    public void deveGerarListaDeClientes(){
        LocalDateTime data = LocalDateTime.of(1999, Month.JANUARY, 21, 19, 30);

        Cliente cliente = new Cliente();
        cliente.setNome("ze");
        cliente.setCpf("54554");
        cliente.setDataNascimento(data);

        Cliente cliente1 = new Cliente();
        cliente1.setNome("joao");
        cliente1.setCpf("5454");
        cliente1.setDataNascimento(data);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);

        ClientePacote clientePacote1 = new ClientePacote();
        clientePacote1.setCliente(cliente1);

        Pacote pacote = new Pacote();
        pacote.pushPacotesClientes(clientePacote1);
        pacote.pushPacotesClientes(clientePacote);

        Assert.assertEquals(pacote.getPacotesClientes().size(),2);

    }

}
