package br.com.dbccompany;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.TipoContratacao;
import br.com.dbccompany.Service.ContratacaoService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Month;

@RunWith( SpringRunner.class )
public class ContratacaoIntegrationTest {

    @MockBean
    ContratacaoService contratacaoService;

    @Test
    public void descontoEOpcional(){
        LocalDateTime data = LocalDateTime.of(1999, Month.JANUARY, 21, 19, 30);
        Espaco espaco = new Espaco();
        espaco.setNome("dbc");
        espaco.setQtdPessoas(250);
        espaco.setValor(800000);

        Cliente cliente = new Cliente();
        cliente.setNome("joao");
        cliente.setCpf("5454");
        cliente.setDataNascimento(data);

        Contratacao contratacao = new Contratacao();

        contratacao.setCliente(cliente);
        contratacao.setEspaco(espaco);
        contratacao.setPrazo(1);
        contratacao.setQuantidade(1);
        contratacao.setTipoContratacao(TipoContratacao.DIARIA);


        contratacaoService.salvar(contratacao);
        contratacaoService.equals(contratacao);
    }

    @Test
    public void calcularValorCorreto(){
        LocalDateTime data = LocalDateTime.of(1999, Month.JANUARY, 21, 19, 30);
        Espaco espaco = new Espaco();
        espaco.setNome("dbc");
        espaco.setQtdPessoas(250);
        espaco.setValor(800);

        Contratacao contratacao = new Contratacao();

        contratacao.setEspaco(espaco);
        contratacao.setQuantidade(2);
        contratacao.setTipoContratacao(TipoContratacao.DIARIA);

        Double result = contratacao.getQuantidade()*contratacao.getEspaco().getValor();

        Assert.assertEquals(1600,result,0.0001);
    }





}
