package br.com.dbccompany;


import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Service.ClienteService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.Month;


@RunWith( SpringRunner.class )
public class ClienteIntegrationTest {

    @MockBean
    ClienteService clienteService;

    @Test
    public void cpfUnico(){
        LocalDateTime data = LocalDateTime.of(1999, Month.JANUARY, 21, 19, 30);

        Cliente cliente = new Cliente();
        cliente.setNome("joao");
        cliente.setCpf("5454");
        cliente.setDataNascimento(data);

        Cliente cliente1 = new Cliente();
        cliente1.setNome("ze");
        cliente1.setCpf("5454");
        cliente1.setDataNascimento(data);

    try {
        clienteService.salvar(cliente);
        clienteService.salvar(cliente1);

    }catch (Exception e){
        Assert.assertEquals("Já existe um usuario com esse CPF!",e.getMessage());
    }
    }

    @Test
    public void cpfVazio(){
        LocalDateTime data = LocalDateTime.of(1999, Month.JANUARY, 21, 19, 30);

        Cliente cliente = new Cliente();
        cliente.setNome("joao");
        cliente.setDataNascimento(data);

        try {
            clienteService.salvar(cliente);

        }catch (Exception e){
            Assert.assertEquals("Campo CPF é obrigatorio!",e.getMessage());
        }
    }

    @Test
    public void nomeVazio(){
        LocalDateTime data = LocalDateTime.of(1999, Month.JANUARY, 21, 19, 30);
        Cliente cliente = new Cliente();
        cliente.setCpf("5454");
        cliente.setDataNascimento(data);

        try {
            clienteService.salvar(cliente);

        }catch (Exception e){
            Assert.assertEquals("Campo Nome é obrigatorio!",e.getMessage());
        }

    }

    @Test
    public void dataVazia(){
        Cliente cliente = new Cliente();
        cliente.setNome("joao");
        cliente.setCpf("5454");
        try {
            clienteService.salvar(cliente);

        }catch (Exception e){
            Assert.assertEquals("Campo data de nascimento é obrigatorio!",e.getMessage());
        }

    }


}
