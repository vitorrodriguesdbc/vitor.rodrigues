package br.com.dbccompany.Entity;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Endereco {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="LOGRADOURO")
	private long logradouro;
	
	@Column(name="NUMERO")
	private long numero;
	
	@Column(name="COMPLEMENTO")
	private long complemento;
	
	@Column(name="BAIRRO")
	private long bairro;
	
	@Column(name="CIDADE")
	private long cidade;
	
	@Column(name="ESTADO")
	private long estado;
	
	@OneToOne(mappedBy = "endereco", cascade = CascadeType.ALL)
    private Agencia agencia ;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(long logradouro) {
		this.logradouro = logradouro;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public long getComplemento() {
		return complemento;
	}

	public void setComplemento(long complemento) {
		this.complemento = complemento;
	}

	public long getBairro() {
		return bairro;
	}

	public void setBairro(long bairro) {
		this.bairro = bairro;
	}

	public long getCidade() {
		return cidade;
	}

	public void setCidade(long cidade) {
		this.cidade = cidade;
	}

	public long getEstado() {
		return estado;
	}

	public void setEstado(long estado) {
		this.estado = estado;
	}
	
	
}
