package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.Entity.Conta;

public interface ContaRepository extends CrudRepository<Conta, Long>{
	Conta findByNumero(long numero);
}
