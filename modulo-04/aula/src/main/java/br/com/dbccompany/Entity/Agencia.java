package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Agencia {
@Id
@GeneratedValue(strategy=GenerationType.SEQUENCE)
private long id;

@Column(name="CODIGO")
private long codigo;

@ManyToOne
@JoinColumn(name="id_banco")
private Banco banco;

@OneToOne
@JoinColumn(name="id_endereco")
private Endereco endereco;

@OneToMany (mappedBy = "agencia", cascade = CascadeType.ALL)
private List<Conta> contas = new ArrayList<>();

public long getId() {
	return id;
}

public void setId(long id) { 
	this.id = id;  
}

public long getCodigo() {  
	return codigo;
}

public void setCodigo(long codigo) {
	this.codigo = codigo;
}

public Banco getBanco() {
	return banco;
}

public void setBanco(Banco banco) {
	this.banco = banco;
}

public Endereco getEndereco() {
	return endereco;
}

public void setEndereco(Endereco endereco) {
	this.endereco = endereco;
}

public List<Conta> getContas() {
	return contas;
}

public void setContas(List<Conta> contas) {
	this.contas = contas;
}


}
