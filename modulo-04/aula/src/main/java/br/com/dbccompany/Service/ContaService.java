package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Repository.ContaRepository;

public class ContaService {
	
	@Autowired
	private ContaRepository contaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Conta buscarConta(Conta conta) {
		
		return contaRepository.save(conta);
		
	}
}
