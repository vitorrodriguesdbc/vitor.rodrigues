package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Repository.ClienteRepository;

public class ClienteService { 
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Cliente buscarCliente(Cliente cliente) {
		
		return clienteRepository.save(cliente);
		
	}
}
