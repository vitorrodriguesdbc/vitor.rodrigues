package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Repository.AgenciaRepository;

@Service
public class AgenciaService {

	@Autowired
	public AgenciaRepository agenciaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia buscarAgencia(Agencia agencia) {
		
		return agenciaRepository.save(agencia);
		
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia salvar(Agencia agencia) {
		
		return agenciaRepository.save(agencia);
		
	}
}
