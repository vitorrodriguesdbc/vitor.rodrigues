package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;

@Entity
public class Cliente {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;

	@Column(name="NOME")
	private String nome;
	
	@Column(name="CPF")
	private long cpf;
	
	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Cliente_conta", joinColumns
            = {
            		@JoinColumn(name = "id_cliente")},
            inverseJoinColumns
            = {
                @JoinColumn(name = "id_conta")})
    private List<Conta> contas = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}
		
}
